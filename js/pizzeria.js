var datos;

async function leerJSON(){
  try {
    let respuesta = await fetch('https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json')
    datos = await respuesta.json();
  } catch (error) {
    console.log(error)   
  }
}

async function obtenerDatos(){
   await leerJSON();
   document.querySelector('#nombre-pizzeria').innerHTML = datos.nombrePizzeria
}

//INICIO FUNCIONES DE O PARA INDEX.HTML =============================================================================
function obtenerCantidadPizzasIngresada(){
  return document.querySelector('#cantidad').value;
}

function crearSeleccionPizzas(){
  
  let cantidad = obtenerCantidadPizzasIngresada();

  if(cantidad > 0){
    document.querySelector('#contenido-seleccion-pizza').innerHTML = formatoContenidoSeleccionPizza(cantidad);
    document.querySelector('#contenido-boton-cargar-nuevo').innerHTML = formatoContenidoBotonCargarDeNuevo();
    crearBotonCrearOpciones();
    desactivarSeleccionPizzas(true);
  }else{
    alert('Ingrese una cantidad valida');
  }

}

function formatoContenidoSeleccionPizza(cantidad){
  let msg = '';

  for(let i=1; i <= cantidad; i++){
    msg += 
    `
    <section class="contenido-seleccion-pizza border border-danger mt-3">
    
      <div class="row">
      
        <div class="col-md-3 text-right mt-3">
          <h4>Tamaño de la pizza ${i}:</h4>
        </div>
        
        <div class="col-md-9 text-left mt-3">
          <select class="custom-select bg-danger text-white" id="pizzaSeleccionada${i}" onclick="crearBotonCrearOpciones()">
            <option value="0">Grande</option>
            <option value="1">Mediano</option>
            <option value="2">Pequeño</option>
          </select>
        </div>
            
      </div>
            
    </section>
    `
  }
  
  return msg;
}


function quitarBotonCrearOpciones(){
  document.querySelector('#contenido-boton-cargar-opciones').innerHTML = '';
}

function formatoBotonCrearOpciones(){
  quitarBotonCrearOpciones();
  return `<div class="text-right">        
             <a href="html/opciones.html?cantidad_pizzas=${obtenerCantidadPizzasIngresada()}&&tamanios=${obtenerTamaniosPizzasSeleccionados()}" class="btn btn-danger">Cargar opciones</a>
          </div>  
          `;
}

function crearBotonCrearOpciones(){
  document.querySelector('#contenido-boton-cargar-opciones').innerHTML = formatoBotonCrearOpciones();
}

function obtenerTamaniosPizzasSeleccionados(){
  let tamanios = [];

  for(let i=1; i <= obtenerCantidadPizzasIngresada(); i++){
    let select = document.querySelector(`#pizzaSeleccionada${i}`);
     tamanios.push(select.options[select.selectedIndex].value);
  }

  return tamanios;
}

function formatoContenidoBotonCargarDeNuevo(){
  return "<button onclick='cargarDeNuevo()' class='btn btn-danger'>Cargar de nuevo</button>";
}

function cargarDeNuevo(){
  document.querySelector('#contenido-seleccion-pizza').innerHTML = '';
  document.querySelector('#contenido-boton-cargar-nuevo').innerHTML = '';
  document.querySelector('#contenido-boton-cargar-opciones').innerHTML = '';
  desactivarSeleccionPizzas(false);
}

function desactivarSeleccionPizzas(desactivar){
  document.querySelector('#cantidad').disabled = desactivar;
  document.querySelector('#btncrear').disabled = desactivar;
}
//FIN DE FUNCIONES DE O PARA INDEX.HTML ************************************************************************************

//INICIO FUNCIONES DE UTILIDAD =============================================================================================
function parametrosURL(){
  let parametros = new URLSearchParams(window.location.search);
  return parametros;
}

function convertirURlArreglo(get){
  return parametrosURL().getAll(get)[0].split(',');
}

function convertirCadenaTextoArreglo(texto){
 return texto.split(',');
}

function eliminarElementoDeArray(array, itemEliminar){
  let indexItem = array.indexOf(itemEliminar);
  if(indexItem > -1){
    array.splice(indexItem, 1);
  }
  return array;
}
//FIN DE FUNCIONES DE UTILIDAD **********************************************************************************************

//INICIO DE FUNCIONES DE O PARA OPCIONES.HTML ==================================================================================
async function obtenerDatosOpciones(){
  await obtenerDatos();
  crearContenidoOpcionesSaboresPizzass();
  crearContenidoBtnFactura();
  console.log(datos)
}

function crearContenidoOpcionesSaboresPizzass(){
  document.querySelector('#contenido-opciones-pizzas').innerHTML = formatoContenidoOpcionesSaboresPizzas();
}

function crearContenidoIngredientes(seleccion, i){
  return `
      <div class="contenido-ingrediente">
        <h6 class="mt-3 text-danger" id="titulo-ingrediente-${seleccion}-${i}"> ${seleccion === 1 ? 'Ingrediente de pizza Napolitana' : ''}</h6>
        <div class="contenido-checkbox-ingredientes" id="contenido-adicionales-${seleccion}-${i}">
         ${seleccion === 1 ? crearFormatoIngredientesAdiccionales(seleccion, i) : ''}
        </div>
      </div>
        `
}

function formatoContenidoOpcionesSaboresPizzas(){
  let msg = '';

  for(let i=1; i <= parametrosURL().get('cantidad_pizzas'); i++){
    msg += `
    <div class="container mt-5 border border-danger">
  
    <div class="row contenido-sabores-pizzas mt-4">

      <div class="col-md-6 text-right">
        <h5>Escoja sabores para pizza ${i} (Puede escoger uno o dos):</h5>
      </div>
  
      <div class="col-md-2 text-right">
        <select class="custom-select bg-danger text-white" name="seleccion_pizza1" id="seleccionPizza1_${i}" onclick="crearNuevoContenidoParaSeleccion2SaboresPizza(${i})">
          ${crearContenidoSaboresPizzas()}
        </select>
      </div>
  
      <div class="col-md-4 text-left">
        <select class="custom-select bg-danger text-white" name="seleccion_pizza2" id="seleccionPizza2_${i}" onclick="obtenerImagenPizzaSeleccion(2, ${i})">          
        <option select value="-1">Ninguno</option>
        ${crearContenidoSaboresPizzas()}
        </select>
      </div>
  
    </div>
  
  <div class="row">
  
    <div class="col-md-6">
  
     ${crearContenidoIngredientes(1, i)}
     ${crearContenidoIngredientes(2, i)}

    </div>
  
    <div class="col-md-3 mb-3 text-left" id="imagen1_${i}"> <img height="150px" width="250px" src="${datos.pizzas[0].url_Imagen}" alt="Napolitana"> </div>
  
    <div class="col-md-3 mb-3 text-left" id="imagen2_${i}"> </div>

  </div>
  </div>
  
    `
  }

  return msg;
}

function obtenerSaboresPizzas(){
  let sabores = [];
 for(let i=0; i < datos.pizzas.length; i++){
  sabores.push(datos.pizzas[i].sabor)
 }
 return sabores;
}

function crearContenidoSaboresPizzas(){
  return formatoSaboresPizzas(obtenerSaboresPizzas())
}

function formatoSaboresPizzas(saboresPizzas){
  let msg = '';

  for(let i= 0; i < saboresPizzas.length; i++){
    msg += `<option value="${i}">${saboresPizzas[i]}</option>`
  }

  return msg;
}

function formatoNuevoContenidoParaSeleccion2SaboresPizza(numeroPizza){
  return `
          <option select value="-1">Ninguno</option>
          ${formatoSaboresPizzas(eliminarElementoDeArray(obtenerSaboresPizzas(), obtenerNombreSaborPizzaSeleccionada(1, numeroPizza)))};
        `
}

function crearNuevoContenidoParaSeleccion2SaboresPizza(numeroPizza){
  document.querySelector(`#seleccionPizza2_${numeroPizza}`).innerHTML = formatoNuevoContenidoParaSeleccion2SaboresPizza(numeroPizza);
  obtenerImagenPizzaSeleccion(1, numeroPizza);
  obtenerImagenPizzaSeleccion(2, numeroPizza);
}

function obtenerSaborPizzaSeleccionada(seleccionPizza, numeroPizza){
  let select = document.querySelector(`#seleccionPizza${seleccionPizza}_${numeroPizza}`);
  return select.options[select.selectedIndex].value;
}

function obtenerNombreSaborPizzaSeleccionada(seleccionPizza, numeroPizza){
  let select = document.querySelector(`#seleccionPizza${seleccionPizza}_${numeroPizza}`);
  return select.options[select.selectedIndex].text;
}

function obtenerCantidadPizzasURL(){
  return parametrosURL().get('cantidad_pizzas');
}

function obtenerSaboresPizzasSeleccionados(){
  let saboresPizzaSeleccionados ='';
  for(let i=1; i <= obtenerCantidadPizzasURL(); i++){
    let select = document.querySelector(`#seleccionPizza1_${i}`);
    let select2 = document.querySelector(`#seleccionPizza2_${i}`);
    let sabores = new Array(2);
    sabores[0] = select.options[select.selectedIndex].value;
    sabores[1] = select2.options[select2.selectedIndex].value;
    saboresPizzaSeleccionados += "sabor_pizza" + i + "="  + sabores + '&&';
  }
  return saboresPizzaSeleccionados;
}

function definirURLImagenPizza(saborPizza){
  console.log(saborPizza)
  return datos.pizzas[saborPizza].url_Imagen;
}

function formatoImagenPizza(seleccionPizza, numeroPizza){
  let saborPizza = obtenerSaborPizzaSeleccionada(seleccionPizza, numeroPizza);
  let nombrePizza = obtenerNombreSaborPizzaSeleccionada(seleccionPizza, numeroPizza);
  crearInformacionContenidoIngredientesAdicionales(seleccionPizza, nombrePizza, numeroPizza);
  if(saborPizza !== '-1'){
    return `<img height="150px" width="250px" src="${definirURLImagenPizza(saborPizza)}" alt="${saborPizza}">`;
  }else{
    return '';
  }
}

function obtenerImagenPizzaSeleccion(seleccionPizza, numeroPizza){
  crearContenidoBtnFactura()
  document.querySelector(`#imagen${seleccionPizza}_${numeroPizza}`).innerHTML = formatoImagenPizza(seleccionPizza, numeroPizza);
}

function obtenerValorSeleccionadoCombobox(id){
  let select = document.querySelector(`seleccionPizza1_${id}`);
  return select.options[select.selectedIndex].value;
}

function crearInformacionContenidoIngredientesAdicionales(seleccion, saborPizza, i){
  let tituloIngrediente = document.querySelector(`#titulo-ingrediente-${seleccion}-${i}`);
  let contenidoIngredientePizza = document.querySelector(`#contenido-adicionales-${seleccion}-${i}`);
  if(saborPizza !== 'Ninguno'){
    tituloIngrediente.innerHTML = `<h6 class="text-danger"> Ingrediente de pizza ${saborPizza} </h6>`;
    contenidoIngredientePizza.innerHTML = crearFormatoIngredientesAdiccionales(seleccion, i);
  }else{
    tituloIngrediente.innerHTML = '';
    contenidoIngredientePizza.innerHTML = '';
  }
}

function obtenerAdiccionales(){
  let dato = []
  for(let i=0; i < datos.adicional.length; i++){
    dato.push(datos.adicional[i].nombre_ingrediente)
  }
  return dato;
}

function crearFormatoIngredientesAdiccionales(seleccion, numeroPizza){
  let msg = '';
  for(let i=0; i < obtenerAdiccionales().length; i++){
    msg +=  formatoAdicionales(seleccion, i, numeroPizza, obtenerAdiccionales()[i])
  }
  return msg;
}

function formatoAdicionales(seleccion, i, numeroPizza, nombre){
  return `
  <div class="form-check ml-3">
  <input class="form-check-input" type="checkbox" name="chkingredientes" value="${i}" id="chk${seleccion}_${i}_${numeroPizza}" onclick="crearContenidoBtnFactura()">
  <label class="form-check-label" for="chk${seleccion}_${i}_${numeroPizza}">
  ${nombre}
  </label>
    </div>
    `
}

function obtenerArregloCheckBox() {

	let checkboxes = document.querySelectorAll('.form-check-input');
	//let checkboxes = querySelectorAll('.form-check-input');
	let arreglo = [];

  let cantPizzas = obtenerCantidadPizzasURL();
  
	for (let x = 1; x <= cantPizzas; x++) {

		for (let i = 0; i < checkboxes.length; i++) {

			if (checkboxes[i].checked && (checkboxes[i].name == `chk${1}_${i}_${x}`)) {
				arreglo.push(checkboxes[i].value + "-" + 1 + "-" + x);
			}


			if (checkboxes[i].checked && (checkboxes[i].name == `chk${2}_${i}_${x}`)) {

				arreglo.push(checkboxes[i].value + "-" + 2 + "-" + x);
			}
		}
	}
	return arreglo;
}

function adicionalesSeleccionados(){
  let ingredientes = [];
  let chk = document.getElementsByName('chkingredientes');
  for(let i= 0; i < chk.length; i++){
    if(chk[i].checked){
      ingredientes.push(chk[i].value);
    }
  }
 
  return ingredientes;
}

function quitarBotonCalcularFactura(){
  document.querySelector('#contenido-btn-calcula-factura').innerHTML = '';
}

function formatoBtnCalculaFactura(){
  quitarBotonCalcularFactura();
  return `<div class="text-center">        
  <a href="factura.html?cantidad_pizzas=${obtenerCantidadPizzasURL()}&&tamanios=${parametrosURL().get('tamanios')}&&adicionales=${adicionalesSeleccionados()}&&${obtenerSaboresPizzasSeleccionados()}" class="btn btn-danger">Calcular facturas</a>
  </div>  
  `;
}

function crearContenidoBtnFactura(){
  document.querySelector('#contenido-btn-calcula-factura').innerHTML = formatoBtnCalculaFactura()
}
//FIN DE FUNCIONES DE O PARA OPCIONES.HTML ****************************************************************************************************

//INCIO FUNCIONES DE O PARA FACTURA.HTML ======================================================================================================
function verTamaniosPizzasSeleccionados(){
  let tamanios = convertirURlArreglo('tamanios');
  for(let i=0; i < tamanios.length; i++){
    console.log(tamanios[i])
  }
}

function verAdicionalesPizzasSeleccionados(){
  let adicionales = convertirURlArreglo('adicionales');
  for(let i=0; i < adicionales.length; i++){
    console.log(adicionales[i])
  }
}

function verSaboresPizzasSeleccionados(){
  for(let i=1; i <= parametrosURL().get('cantidad_pizzas'); i++){
    console.log(convertirURlArreglo(`sabor_pizza${i}`))
  }
}


function prueba(){
  let msg = '';
  msg += "<h3 class='text-center'>Pizzas:</h3> <hr>"

  msg += `
  <div class="row">
  <div class="col-md-6">
    <h4 class='text-danger'>Descripción</h4>
  </div>

  <div class="col-md-6 text-right">
   <h4 class='text-danger'>Valor</h4>
  </div>
  </div>
  `

  for(let i=0; i < pruebaSabores()[1].length; i++){
    msg += `
    <div class="row">
    <div class="col-md-6">
      ${pruebaSabores()[0][i]}
    </div>

    <div class="col-md-6 text-right">
      ${pruebaSabores()[1][i]}
    </div>
  </div>
    `

  }

  msg += `
  <hr>
  <h5 class='text-danger text-right'>${formatter.format(pruebaSabores()[2])}</h5>
  `

  msg += "<h3 class='text-center'>Adicionales:</h3> <hr>"

  msg += `
  <div class="row">
  <div class="col-md-6">
    <h4 class='text-danger'>Descripción</h4>
  </div>

  <div class="col-md-6 text-right">
   <h4 class='text-danger'>Valor</h4>
  </div>
  </div>
  `

  for(let i=0; i < pruebaAdicionales()[1].length; i++){
    msg += `
    <div class="row">
    <div class="col-md-6">
      ${pruebaAdicionales()[0][i]}
    </div>

    <div class="col-md-6 text-right">
      ${pruebaAdicionales()[1][i]}
    </div>
  </div>
    `

  }

  msg += `
  <hr>
  <h5 class='text-danger text-right'> ${formatter.format(pruebaAdicionales()[2])}</h5>
  `
  
  msg += `
  <hr>
  <h3 class='text-danger text-right'>Total: ${formatter.format(totalPagar())}</h3>
  `
  
  return msg;
}

const formatter = new Intl.NumberFormat('es-CO', {
  style: 'currency',
  currency: 'COP',
  minimumFractionDigits: 0
})

function pruebaSabores(){
  let sabores = [];
  let precios = [];
  let total = 0;
  for(let i=1; i <= parametrosURL().get('cantidad_pizzas'); i++){
    let indexSabor1 = convertirURlArreglo(`sabor_pizza${i}`)[0];
    let indexSabor2 = convertirURlArreglo(`sabor_pizza${i}`)[1];
    let sabor1 = obtenerSaborPizzaDatos(indexSabor1);
    let sabor2 = obtenerSaborPizzaDatos(indexSabor2);
    let sabor1Mostrar = '';

    if(sabor2 == ''){
      sabor1Mostrar = sabor1;
    }else{
      sabor1Mostrar = 'Mitad ' + sabor1;
    }
    let sabor2Mostrar = (sabor2 == '' ? '' : ' y Mitad ' + sabor2);
    let indexTamanio = convertirURlArreglo('tamanios')[i - 1];
    let precioSabor2 = (indexSabor2 == -1) ? 0 : obtenerPrecioPizzaDatos(indexSabor2, indexTamanio);

    let totalPrecio = totalPrecioPizza(obtenerPrecioPizzaDatos(indexSabor1, indexTamanio), precioSabor2);
    sabores.push("Pizza " + obtenerTamanioPizzaDatos(0, indexTamanio) + " " + sabor1Mostrar + sabor2Mostrar);
    precios.push(formatter.format(totalPrecio));
    total += totalPrecio;
  }
  console.log("Total a pagar: " + total)
  let datos = new Array(3);
  datos[0] = sabores;
  datos[1] = precios;
  datos[2] = total;
  return datos;
}

function totalPagar(){
  return pruebaSabores()[2] + pruebaAdicionales()[2];
}

function pruebaAdicionales(){
let descripcion = [];
let valor = [];
let adicionales = new Array(3);

let total = 0;

let indexAdicionales = convertirURlArreglo('adicionales');

if(indexAdicionales != ''){
  for(let i=0; i < indexAdicionales.length; i++){
    descripcion.push("Adicional " + nombreAdicionalPizza(indexAdicionales[i])) ;
    valor.push(formatter.format(precioAdiccionalPizza(indexAdicionales[i])));
    total += precioAdiccionalPizza(indexAdicionales[i]);
  }
}
adicionales[0] = descripcion;
adicionales[1] = valor;
adicionales[2] = total;
return adicionales;
}

function nombreAdicionalPizza(indexAdicional){
  return datos.adicional[indexAdicional].nombre_ingrediente;
}

function precioAdiccionalPizza(indexAdicional){
  return datos.adicional[indexAdicional].valor;
}

function totalPrecioPizza(precioPizza1, precioPizza2){
  return (precioPizza1 > precioPizza2) ? precioPizza1 : precioPizza2;
}

function obtenerTamanioPizzaDatos(indexSabor, indexTamanio){
  return datos.pizzas[indexSabor].precio[indexTamanio].tamano;
}

function obtenerSaborPizzaDatos(indexSabor){
  return (indexSabor == -1) ? '': datos.pizzas[indexSabor].sabor;
}

function obtenerPrecioPizzaDatos(indexSabor, indexTamanio){
  return datos.pizzas[indexSabor].precio[indexTamanio].precio;
}

async function obtenerDatosFactura(){
  await obtenerDatos();
  console.log(datos)
  verTamaniosPizzasSeleccionados();   
  verSaboresPizzasSeleccionados();
  console.log(prueba())
  document.querySelector('#contenido-factura').innerHTML = prueba();
}
//FIN DE FUNCIONES DE O PARA FACTURA.HTML **************************************************************************************************