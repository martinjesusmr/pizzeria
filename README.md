![Pizzería](http://www.madarme.co/portada-web.png)
# Título del proyecto:

## Ejercicio Pizzería La QQTEÑA 🍕 

## Índice 📜
1. [Características](#-características)
2. [Contenido del proyecto](#-contenido-del-proyecto)
3. [Tecnologías](#%EF%B8%8F-tecnologías-)
4. [IDE](#%EF%B8%8F-ide)
5. [Instalación](#%EF%B8%8F-instalación)
6. [Demo](#-demo)
7. [Autor](#-autor)
8. [Institución Académica](#-institución-académica)

## 💡 Características: 

- Creación dinámica de componentes de HTML con Javascript
- Uso del metodo URLSearchParams de JavaScript para la lectura de datos enviados por la URL
- Envío de información por la URL con Javascript y HTML
- Lectura de datos json a través de la API fecth JavaScript
- Carga dinámica del JSON 
- Archivo json de ejemplo: [ver](https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json)


## 📝 Contenido del proyecto:
- [index.html](<index.html>): Archivo principal de la página web. Donde se ingresa la cantidad de pizzas y se crean las opciones de los tamanios y los envio a opciones.html.
- [opciones.html](<html/opciones.html>): Archivo que recibe la cantidad de pizzas y los tamaños enviados desde la página principal index.html. En esta página selecciono los sabores de cada pizza y los adicionales correspondientes y los envio a factura.html.
- [factura.html](<html/factura.html>): Archivo que recibe toda la información enviada por opciones.html como los tamaños de la pizza, la cantidad de pizzas y los adicionales de cada pizza. Con la información obtenida por la URL se muestra la descripción de lo que se selecciono y su respectivo valor o precio.
- [js/pizzeria.js](<js/pizzeria.js>): Archivo que contiene la lógica de la página donde estan todas las funciones como lectura de JSON y el envio de datos por medio de la URL


## 🛠️ Tecnologías :

- Bootstrap 4.4
- CSS 3
- HTML 5
- JavaScript


## 🖥️ IDE:

El proyecto se desarrolla usando visual studio code. 


## ⚙️ Instalación:

Se necesita un navegador web para utilizar la página web. Se recomienda google chrome, microsoft edge o firefox.

Para poner en funcionamiento la Página tiene que ingresar a index.html.

## 🔍 Demo:
Para ver el demo de la aplicación puede dirigirse a: [Pizzeria](http://ufps24.madarme.co/Pizzeria/).

## Ejemplo de funcionamiento:

### Selección de la cantidad de Pizzas que quiere comprar y su respectivos tamaños:
![Pizzería](https://gitlab.com/martinjesusmr/pantallazos/-/raw/master/pizzeria/index.PNG)
***

### Selección de los sabores y de adicionales para cada Pizza:
![Pizzería](https://gitlab.com/martinjesusmr/pantallazos/-/raw/master/pizzeria/opciones.PNG)
***

### Facturación donde se muestra la descripción de las pizzas que se compro, los adicionales y su respectivos valores:
![Pizzería](https://gitlab.com/martinjesusmr/pantallazos/-/raw/master/pizzeria/factura.PNG)


## 🧑 Autor:
Proyecto desarrollado por [Martin de Jesus Medina Ruvian](<https://martinmedinaruvian.github.io/index/>).
Código: 1151791.

## 🏫 Institución Académica:  
Proyecto desarrollado en la Materia programación web del Programa de Ingeniería de Sistemas de la Universidad Francisco de Paula Santander.
